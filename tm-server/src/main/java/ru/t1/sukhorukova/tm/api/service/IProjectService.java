package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(
            @Nullable final String userId,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project updateById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project updateByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    );

    @NotNull
    Project changeProjectStatusById(
            @NotNull final String userId,
            @NotNull String id,
            @NotNull Status status
    );

    @NotNull
    Project changeProjectStatusByIndex(
            @NotNull final String userId,
            @NotNull Integer index,
            @NotNull Status status
    );

    boolean existsById(
            @NotNull final String userId,
            @NotNull String id
    );

}
