package ru.t1.sukhorukova.tm.api.service;

import ru.t1.sukhorukova.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
