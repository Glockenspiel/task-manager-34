package ru.t1.sukhorukova.tm.api.repository;

import ru.t1.sukhorukova.tm.model.Project;

public interface IProjectRepository extends IUserOwnerRepository<Project> {
}
