package ru.t1.sukhorukova.tm.api.repository;

import ru.t1.sukhorukova.tm.model.Session;

public interface ISessionRepository extends IUserOwnerRepository<Session> {
}
