package ru.t1.sukhorukova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sukhorukova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.sukhorukova.tm.command.AbstractCommand;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sukhorukova.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectEndpoint getProjectEndpoint() {
        return getLocatorService().getProjectEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

}
