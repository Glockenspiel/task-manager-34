package ru.t1.sukhorukova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.project.ProjectStartByIdRequest;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-start-by-id";

    @NotNull
    public static final String DESCRIPTION = "Start project by id.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");

        System.out.println("Enter project id:");
        @Nullable final String projectId = TerminalUtil.nextLine();

        @Nullable final ProjectStartByIdRequest request = new ProjectStartByIdRequest(getToken());
        request.setProjectId(projectId);
        getProjectEndpoint().startByIdProject(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
