package ru.t1.sukhorukova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

}
