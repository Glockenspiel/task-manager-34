package ru.t1.sukhorukova.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.model.User;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractUserResponse {

    public UserLogoutResponse(@Nullable User user) {
        super(user);
    }

}
